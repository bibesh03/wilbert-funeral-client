import React from 'react';
import Navbar from './components/Navbar';
import DashboardComponent from './components/Dashboard';

const App = (props) => {
  return (
    <>
      <Navbar/>
      <DashboardComponent />
     </>
  );
}

export default App;
