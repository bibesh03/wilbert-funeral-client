import React, { useState, useEffect } from 'react';
import { Menu, Container, Button, Dropdown, Icon } from "semantic-ui-react";
import { getCurrentUser, logOut } from '../_services/userService';
import { Link } from 'react-router-dom';

const Navbar = (props) => {
  const [userDetails, setUserDetails] = useState({ firstName: "", lastName: "", isAdmin: false, username: ""});
  useEffect(() => {
    setUserDetails(getCurrentUser());
  }, [])

  const logOutUser = () => {
    logOut(getCurrentUser().id);
    localStorage.removeItem('user');
    props.history.push("/login");
  }

  const trigger = (
    <span>
      <Icon name='user' /> Hello, {userDetails.username}
    </span>
  )
  
  const options = [
    {
      key: 'user',
      text: (
        <span>
          Signed in as <strong>{userDetails.firstName + " " + userDetails.lastName}</strong>
        </span>
      ),
      disabled: true,
      visible: 'ALL'
    },
    { key: 'profile', text: ( <Link to="/Manage" style={{color: 'black'}}>Manage Users</Link>), visible: 'ADMIN'},
    { key: 'sign-out', text: (<span onClick={() => logOutUser()}>Sign Out</span>), visible: 'ALL' },
  ]

  return (
    <Menu inverted>
      <Container>
        <Menu.Item header position='left'>
          <Link to="/">Wilbert Funeral Home</Link>
        </Menu.Item>
        
        <Menu.Item position='right'>
          <Dropdown trigger={trigger} options={userDetails.isAdmin ? options.map((x) => ({
            key: x.key,
            text: x.text
          })) : options.filter(x => x.visible === 'ALL').map((x) => ({
            key: x.key,
            text: x.text
          }))}
          pointing
          />
        </Menu.Item>
      </Container>
    </Menu>
  )
}

export default Navbar;
