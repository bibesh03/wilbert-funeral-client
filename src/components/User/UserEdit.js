import React, { useState, useEffect } from 'react'
import { Formik } from 'formik'
import UserEditValidation from '../../_helpers/validators/Users/UserEditValidation'
import { Grid, Button, Form, Header, Container, Icon } from 'semantic-ui-react'
import { Link } from 'react-router-dom';
import { getUserId, editUser,resetPassword } from '../../_services/userService'
import { Alert } from '../../_helpers/Alert';

const UserEdit = (props) => {
  const [userToEdit, setUserToEdit] = useState({ firstName: "", lastName: "", userName: '', email: '', isActive:false, isUserAdmin: false });

  useEffect(() => {
    getUserId(props.match.params.id).then(res => {
      setUserToEdit(res);
    });
  }, []);

  const saveEditedUser = (editedUser, setSubmitting) => {
    editUser(editedUser).then(res => {
      setSubmitting(false);
      if(res.success)
        props.history.push('/Manage');
     
      Alert(res);
    });
  }

  const changePasswordHandler = () => {
    resetPassword(props.match.params.id).then(res=> {
      Alert(res);
    })
  }

  return (
    <Container>
      <Formik initialValues={userToEdit}
        validationSchema={UserEditValidation}
        enableReinitialize
        onSubmit={(values, { setSubmitting }) => {
            setUserToEdit(values);
            saveEditedUser(values, setSubmitting);
        }}>
        {({ values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting }) => (
            <React.Fragment>
              <Header as='h2'>
                <Header.Content>
                  Edit User Details
                  <Header.Subheader>
                    <Link to='/Manage'> Back to listing</Link>
                  </Header.Subheader>
                </Header.Content>
                <Button floated='right' color='google plus' onClick={changePasswordHandler}><Icon name='key'/>Reset User Password</Button>
              </Header>
              <Form onSubmit={handleSubmit}>
                <Grid divided='vertically'>
                  <Grid.Row columns={2}>
                    <Grid.Column>
                      <Form.Group widths='equal'>
                        <Form.Field>
                          <label>FirstName</label>
                          <Form.Input
                            error={touched.firstName && errors.firstName}
                            fluid
                            placeholder='FirstName'
                            type='text'
                            id='firstName'
                            onChange={handleChange}
                            value={values.firstName}
                            onBlur={handleBlur}
                            className={touched.firstName && errors.firstName ? "has-error" : null}
                          />
                        </Form.Field>
                      </Form.Group>
                    </Grid.Column>

                    <Grid.Column>
                      <Form.Group widths='equal'>
                        <Form.Field>
                          <label>LastName</label>
                          <Form.Input placeholder='LastName'
                            error={touched.lastName && errors.lastName}
                            fluid
                            type='text'
                            id='lastName'
                            onChange={handleChange}
                            value={values.lastName}
                            onBlur={handleBlur}
                            className={touched.lastName && errors.lastName ? "has-error" : null} />
                        </Form.Field>
                      </Form.Group>
                    </Grid.Column>

                    <Grid.Column>
                      <Form.Group widths='equal'>
                        <Form.Field>
                          <label>Username</label>
                          <Form.Input placeholder='Username'
                            error={touched.userName && errors.userName}
                            fluid
                            type='text'
                            id='userName'
                            onChange={handleChange}
                            value={values.userName}
                            onBlur={handleBlur}
                            className={touched.userName && errors.userName ? "has-error" : null} />
                        </Form.Field>
                      </Form.Group>
                    </Grid.Column>

                    <Grid.Column>
                      <Form.Group widths='equal'>
                        <Form.Field>
                          <label>Email</label>
                          <Form.Input placeholder='Email'
                            error={touched.email && errors.email}
                            fluid
                            type='text'
                            id='email'
                            onChange={handleChange}
                            value={values.email}
                            onBlur={handleBlur}
                            className={touched.email && errors.email ? "has-error" : null} />
                        </Form.Field>
                      </Form.Group>
                    </Grid.Column>

                    <Grid.Column>
                      <Form.Group widths='equal'>
                        <Form.Field>
                          <Form.Field>
                            <Form.Checkbox 
                              label='Is Admin?'
                              id='isUserAdmin'
                              onChange={handleChange}
                              checked={values.isUserAdmin}
                            />
                          </Form.Field>
                        </Form.Field>
                      </Form.Group>
                    </Grid.Column>

                    <Grid.Column>
                      <Form.Group widths='equal'>
                        <Form.Field>
                          <Form.Field>
                            <Form.Checkbox 
                              label='Is Active?'
                              id='isActive'
                              onChange={handleChange}
                              checked={values.isActive}
                            />
                          </Form.Field>
                        </Form.Field>
                      </Form.Group>
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
                <Button type='submit' color='linkedin' size={'medium'} disabled={isSubmitting} floated='right'> <Icon name='save'/> Save</Button>
              </Form>
            </React.Fragment>
          )}
      </Formik>
    </Container>
  );
}
export default UserEdit;
