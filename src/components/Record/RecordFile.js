import React, { useEffect, useState } from "react";
import { PDFReader } from "reactjs-pdf-view";
import {
  Button,
  Header,
  Icon,
} from "semantic-ui-react";
import { viewFile} from "../../_services/recordService";
import constants from "../../_constants/constant";

const RecordFile = (props) => {
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPage, setTotalPage] = useState(1);
  const [zoomState, setZoomState] = useState(1.5);
  useEffect(() => {
    // viewFile(props.match.params.id).then((res) => {
    // });
  }, []);

  return (
    <div>
      <Header as="h3">
        {currentPage} / {totalPage}&nbsp;&nbsp;&nbsp;
        {zoomState}
      </Header>
      <Button
        size='small' 
        color="linkedin"
        disabled={currentPage === 1}
        onClick={() => setCurrentPage(currentPage - 1)}
      >
        <Icon name="arrow left" /> Prev
      </Button>
      <Button
        color="linkedin"
        size='small'
        disabled={currentPage === totalPage}
        onClick={() => setCurrentPage(currentPage + 1)}
      >
        <Icon  name="arrow right" /> Next
      </Button>

      <Button
        floated="right"
        color="linkedin"
        onClick={() => {setZoomState(zoomState - 1);
        }}
      >
        <Icon name="zoom out" /> ZoomOut
      </Button>
      <Button
        floated="right"
        color="linkedin"
        onClick={() => setZoomState(zoomState + 1)}
      >
        <Icon name="zoom in" /> ZoomIn
      </Button>

      <PDFReader
        scale={zoomState}
        url={decodeURIComponent(
          constants.API_URL + "Records/file/" + props.match.params.id
        )}
        page={currentPage}
        onDocumentComplete={(totalPage) => setTotalPage(totalPage)}
      />
      <Button 
      floated='right'
      size="small"
      color="linkedin"
      
      
      >
        <Icon name="download"/>Download
      </Button>
      <br />
      <br />
      <br />
    </div>
  );
};
export default RecordFile;
