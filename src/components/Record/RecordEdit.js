import React, { useState, useEffect } from "react";
import { Formik } from "formik";
import RecordEditValidation from "../../_helpers/validators/Records/RecordsNameValidator";
import {
  Grid,
  Button,
  Form,
  Header,
  Container,
  Icon,
  Modal,
  Card,
} from "semantic-ui-react";
import { Link } from "react-router-dom";
import { Alert } from "../../_helpers/Alert";
import { getRecord, editRecord, deleteRecord } from "../../_services/recordService";
import { DateInput } from "semantic-ui-calendar-react";
import Uppy from "@uppy/core";
import "@uppy/core/dist/style.css";
import "@uppy/dashboard/dist/style.css";
import RecordFile from "./RecordFile";
import { Dashboard as UppyDashboard } from "@uppy/react";

const RecordEdit = (props) => {

  const [uppy_Single, setUppySingle] = useState(new Uppy());
  const [showModel_single, setShowModel_single] = useState(false);
  const [recordToEdit, setRecordToEdit] = useState({
    id: "",
    name: "",
    key: "",
    date: "",
  });
  const [fileModel, setFileModel] = useState({
    id: "",
    name: "",
    key: "",
    date: "",
    file: null,
  });

  useEffect(() => {
    getRecord(props.match.params.id).then((res) => {
      setRecordToEdit(res);
      setFileModel({ ...res, file: null });
    });

    setUppySingle(
      new Uppy({
        autoProceed: false,
        restrictions: {
          maxNumberOfFiles: 1,
          minNumberOfFiles: 1,
          allowedFileTypes: [".TIF", ".pdf"],
        },
        onBeforeFileAdded: (currentFile, files) => {
          updateFile(currentFile);
        },
      })
    );
  }, []);

  const saveEditedRecord = (editedRecord, setSubmitting) => {
    editRecord(convertJsonToFormData(editedRecord)).then((res) => {
      setSubmitting(false);
      if (res.success) {
        Alert(res);
      } else {
        Alert(res);
      }

      props.history.push("/");
    });
  };

  const updateFile = (file) => {
    let fileName = file.name.split("_");
    if (fileName.length === 3) {
      getRecord(props.match.params.id).then((res) => {
        setFileModel({
        ...res, file: file.data
        });
      })
    }
  };

  const convertJsonToFormData = (json) => {
    var form_data = new FormData();
    for (var key in json) {
      form_data.append(key, json[key]);
    }
    return form_data;
  };

  return (
    <Container>
      <Formik
        initialValues={fileModel}
        validationSchema={RecordEditValidation}
        enableReinitialize
        onSubmit={(values, { setSubmitting }) => {
          setFileModel(values);
          saveEditedRecord(values, setSubmitting);
        }}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
        }) => (
          <React.Fragment>
            <Header as="h2">
              <Header.Content>
                Edit Records Details
                <Header.Subheader><br/>
                  <Link to="/"><Button color='black' size='small'><Icon name='arrow left' color='white'/>Back</Button></Link>
                </Header.Subheader>
              </Header.Content>
              <Modal
                trigger={<Button floated="right" color='red' size='small'><Icon name='trash' color='white'/>Delete</Button>}
                header='Delete Record'
                content='Are you sure you want to delete this record?'
                actions={['Close', { 
                  key: 'done', 
                  content: 'Delete', 
                  positive: true,
                  onClick: () => {
                    deleteRecord(recordToEdit.id).then((res) => {
                      Alert(res);
                      if (res.success) 
                       props.history.push('/');
                    });
                  } 
                }]}
              />
            </Header>
            <Form onSubmit={handleSubmit}>
              <Grid divided="vertically">
                <Grid.Row columns={3}>
                  <Grid.Column>
                    <Form.Group widths="equal">
                      <Form.Field>
                        <label>Name</label>
                        <Form.Input
                          placeholder="Name"
                          error={touched.name && errors.name}
                          fluid
                          type="text"
                          id="name"
                          onChange={handleChange}
                          value={values.name}
                          onBlur={handleBlur}
                          className={
                            touched.name && errors.name ? "has-error" : null
                          }
                        />
                      </Form.Field>
                    </Form.Group>
                  </Grid.Column>

                  <Grid.Column>
                    <Form.Group widths="equal">
                      <Form.Field>
                      <DateInput
                        label="Date"
                        placeholder="Date"
                        value={values.date}
                        iconPosition="left"
                        popupPosition='right center'
                        onChange={(e, data) => {
                          setFileModel({ ...fileModel, date: data.value });
                        }}
                        dateFormat={"YYYY-MM-DD"}
                        id="date"
                        error={touched.date && errors.date}
                        className={
                          touched.date && errors.date ? "has-error" : null
                        }
                        closable
                      />
                    </Form.Field>
                    </Form.Group>
                  </Grid.Column>
                  <Grid.Column>
                    <Form.Group widths="equal">
                      <Form.Field>
                        <label>Key</label>
                        <Form.Input
                          placeholder="Key"
                          error={touched.key && errors.key}
                          fluid
                          type="text"
                          id="key"
                          onChange={handleChange}
                          value={values.key}
                          onBlur={handleBlur}
                          className={
                            touched.key && errors.key ? "has-error" : null
                          }
                        />
                      </Form.Field>
                    </Form.Group>
                  </Grid.Column>
                </Grid.Row>
              </Grid>

              <Button
                type="submit"
                color="green"
                size={"medium"}
                disabled={isSubmitting}
                floated="right"
              >
                <Icon name="save" /> Save
              </Button>
            </Form>
            <Modal
              trigger={
                <Button
                  type="download"
                  color="linkedin"
                  size={"medium"}
                  floated="right"
                  onClick={() => setShowModel_single(true)}
                >
                  <Icon name="upload" /> Replace Document
                </Button>
              }
              open={showModel_single}
              onOpen={() => setShowModel_single(true)}
              onClose={() => {
                setShowModel_single(false);
                uppy_Single.cancelAll();
              }}
            >
              <Modal.Header>Replace file</Modal.Header>
              <Modal.Content>
                <Modal.Description>
                  <Card centered fluid>
                    <UppyDashboard
                      height="300px"
                      width="930px"
                      uppy={uppy_Single}
                      theme="light"
                      showSelectedFiles
                      closeModalOnClickOutside
                      hideProgressAfterFinish
                      hideUploadButton
                    />
                    <Button
                      type="submit"
                      color="green"
                      floated="right"
                      onClick={() => {
                        setShowModel_single(false);
                      }}
                    ><Icon name = 'sync'></Icon>
                      Update
                    </Button>
                  </Card>
                </Modal.Description>
              </Modal.Content>
            </Modal>
          </React.Fragment>
        )}
      </Formik>
    </Container>
  );
};

export default RecordEdit;
