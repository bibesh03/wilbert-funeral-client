import React, { useEffect, useState } from "react";
import DataTable from "react-data-table-component";
import {
  Button,
  Container,
  Header,
  Grid,
  Icon,
  Modal,
  Input,
  Form,
  Card,
  Loader,
} from "semantic-ui-react";
import Uppy from "@uppy/core";
import "@uppy/core/dist/style.css";
import "@uppy/dashboard/dist/style.css";
import { Dashboard as UppyDashboard } from "@uppy/react";
import { DateInput } from "semantic-ui-calendar-react";
import {
  saveFileToServer,
  getAllRecords,
} from "../_services/recordService";
import { Alert } from "../_helpers/Alert";
import Validation from "../_helpers/validators/Records/RecordsNameValidator";
import { Formik } from "formik";
import constants from "../_constants/constant";

const Dashboard = (props) => {
  const [recordList, setRecordList] = useState([
    { id: "", name: "", date: "", key: "" },
  ]);
  const [fileModel, setFileModel] = useState({
    name: "",
    date: "",
    key: "",
    file: null,
  });
  const [showModel_single, setShowModel_single] = useState(false);
  const [showModel_batch, setShowModel_batch] = useState(false);
  const [uppy_Single, setUppySingle] = useState(new Uppy());
  const [uppy_Batch, setUppyBatch] = useState(new Uppy());
  const [searchableList, setSearchableList] = useState(recordList);
  const [filters, setFilters] = useState({ name: "", date: "" });
  const [isUploadingBatch,setIsUploadingBatch] = useState(false);
  const [filterDateRange, setFilterDateRange] = useState({
    lower: "",
    upper: "",
  });
  const loggedInUser = JSON.parse(localStorage.getItem('user'))
  const [isUserAdmin, setIsUserAdmin] = useState(loggedInUser.isAdmin);
  
  useEffect(() => {
    setUppySingle(
      new Uppy({
        autoProceed: false,
        restrictions: {
          maxNumberOfFiles: 1,
          minNumberOfFiles: 1,
          allowedFileTypes: [".TIF", ".pdf"],
        },
        onBeforeFileAdded: (currentFile, files) => {
          validateFileName(currentFile);
        },
      })
    );

    setUppyBatch(
      new Uppy({
        autoProceed: false,
        allowMultipleUploads: true,
        restrictions: {
          maxNumberOfFiles: 10000,
          minNumberOfFiles: 1,
          allowedFileTypes: [".TIF", ".pdf"],
        },
        onBeforeFileAdded: (currentFile, files) => {
        },
      })
    );

    GetRecords();
  }, []);

  const GetRecords = () => {
    getAllRecords().then((res) => {
      setRecordList(res);
      setSearchableList(res);
    });
  };

  const validateFileName = (file) => {
    let fileName = file.name.split("_");

    if (fileName.length === 3) {
      setFileModel({
        name: capital_letter(fileName[0]),
        date: fileName[1],
        key: fileName[2].split(".")[0],
        file: file.data,
      });
    }
  };

  const columns = [
    {
      name: "",
      selector: "id",
      sortable: true,
      cell: (row) => (
        <React.Fragment>
       
          <Button
            color="linkedin"
            size={"mini"}
            onClick={() => window.open(constants.API_URL + "Records/file/" + row.id, "_blank")}
          >
            <Icon name="eye" />
            View
          </Button>
          { isUserAdmin ?
          <Button
            color="facebook"
            size={"mini"}
            onClick={() =>
              props.history.push({
                pathname: "Record/" + row.id,
              })
            }
          >
            <Icon name="edit" />
            Edit
          </Button> : "" 
          }
        </React.Fragment>
      ),
    },
    {
      name: "Name",
      selector: "name",
      sortable: true,
      highlightOnHover: true,
      width: 90
    },
    {
      name: "Date",
      selector: "date",
      sortable: true,
      filter: true,
      highlightOnHover: true,
    },
    // {
    //   name: "Key",
    //   selector: "key",
    //   sortable: true,
    //   filter: true,
    //   highlightOnHover: true,
    // }
  ];

  const convertJsonToFormData = (json) => {
    var form_data = new FormData();
    for (var key in json) {
      form_data.append(key, json[key]);
    }
    return form_data;
  };

  const submitHandler = (fileModel, setSubmitting) => {
    if (uppy_Single.getFiles().length === 0) {
      Alert({
        success: false,
        message: "Error! Please upload a file before submitting",
      });
      return;
    }

    saveFileToServer(convertJsonToFormData(fileModel)).then((res) => {
      Alert(res);
      setSubmitting(false);
      if (res.success) {
        GetRecords();
        setShowModel_single(false);
        uppy_Single.cancelAll();
        setFileModel({ name: "", date: "", key: "", file: null });
      }
    });
  };

  const capital_letter = (str) => 
  {
      str = str.split(" ");
      for (var i = 0, x = str.length; i < x; i++) {
          str[i] = str[i][0].toUpperCase() + str[i].substr(1).toLowerCase();
      }
  
      return str.join(" ");
  }

  const submitHandlerForBatch = () => {
    if (uppy_Batch.getFiles().length <= 1) {
      Alert({
        success: false,
        message: "Error! Please upload two or more files",
      });
      return;
    }
    let areFilesValid = true;
    let validFiles = [];
    let invalidFileName = "";
    for (var x of uppy_Batch.getFiles()) {
      let fileNameArray = x.name.split("_");
      if (fileNameArray.length !== 3 || isNaN(new Date(fileNameArray[1]).getDate())) {
        areFilesValid = false;
        invalidFileName = fileNameArray[0].toUpperCase() + "_" + fileNameArray[1] + "_" + fileNameArray[2];
        break;
      }
      
      validFiles.push({
        name: capital_letter(fileNameArray[0]),
        date: fileNameArray[1],
        key: fileNameArray[2].split(".")[0],
        file: x.data,
      });
    }

    if (areFilesValid) {
      setIsUploadingBatch(true);
      UploadBatchFiles(validFiles);
    } else {
      Alert({
        success: false,
        message:
          "One or more file names are not valid. Please upload files with valid name (eg: name_date_key). File with invalid name: " + invalidFileName,
      });
    }
  };

  const UploadBatchFiles = (validFiles) => {
    let allFilesUploaded = true;
    let noOfFilesUploaded = 0;

    validFiles.forEach((x, i) => {
      saveFileToServer(convertJsonToFormData(x)).then((res) => {
        ++noOfFilesUploaded;
        if (!res.success) {
          allFilesUploaded = false;
          Alert(res);
        }

        let isUploadComplete = noOfFilesUploaded === validFiles.length;
        if (isUploadComplete && allFilesUploaded) {
          Alert({
            success: true,
            message: "Success! Files with valid names have been Uploaded",
          });
        } else if (isUploadComplete && !allFilesUploaded) {
          Alert({
            success: false,
            message: "Could not upload some files",
          });
        } 
        
        if (isUploadComplete) {
          uppy_Batch.cancelAll();
          setShowModel_batch(false);
          GetRecords();
          setIsUploadingBatch(false);
        }
      });
    });
  };

  const filterRecord = (e) => {
    let filteredList = recordList;
    let from = new Date(filterDateRange.lower).getTime();
    let to = new Date(filterDateRange.upper).getTime();

    if (filterDateRange.lower !== "" && filterDateRange.upper !== "") {
      filteredList = filteredList.filter(
        (x) => new Date(x.date).getTime() >= from && new Date(x.date).getTime() <= to
      );
    }

    if (filters.name !== "" && filters.date !== "") {
      filteredList = filteredList.filter(
        x => x.name.toLowerCase().indexOf(filters.name.trim().toLowerCase()) > -1 && x.date.indexOf(filters.date.trim()) > -1
      );
    }

    if (filters.name !== "" && filters.date === "") {
      filteredList = filteredList.filter(
        (x) => x.name.toLowerCase().indexOf(filters.name.trim().toLowerCase()) > -1
      );
    }

    if (filters.date !== "" && filters.name === "") {
      filteredList = filteredList.filter(
        (x) => x.date.indexOf(filters.date.trim()) > -1
      );
    }
    
    setSearchableList(filteredList)
  }

  return (
    <Container>
      
      {/* Single File Upload Uppy Dashboard */}
      <Formik
        initialValues={fileModel}
        enableReinitialize={true}
        validationSchema={Validation}
        onSubmit={(values, { setSubmitting }) => {
          setFileModel(values);
          submitHandler(values, setSubmitting);
        }}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
        }) => (
            <Modal
              title="Modal_Single"
              open={showModel_single}
              onOpen={() => setShowModel_single(true)}
              onClose={() => {
                setShowModel_single(false);
                setFileModel({ name: "", date: "", key: "", file: null });
                uppy_Single.cancelAll();
              }}
            >
              <Modal.Header>Upload a file</Modal.Header>
              <Modal.Content>
                <Modal.Description>
                  <Form onSubmit={handleSubmit}>
                    <Form.Group widths="equal">
                      <Form.Field
                        control={Input}
                        label="Name"
                        error={touched.name && errors.name}
                        value={values.name}
                        placeholder="Name"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        className={
                          touched.name && errors.name ? "has-error" : null
                        }
                        id="name"
                      />
                      <DateInput
                        label="Date"
                        placeholder="Date"
                        value={values.date}
                        iconPosition="left"
                        onChange={(e, data) => {
                          setFileModel({ ...fileModel, date: data.value });
                          
                        }}
                        onBlur={handleBlur}
                        dateFormat={"YYYY-MM-DD"}
                        id="date"
                        error={touched.date && errors.date}
                        className={
                          touched.date && errors.date ? "has-error" : null
                        }
                      />
                      <Form.Field
                        control={Input}
                        label="Key"
                        error={touched.key && errors.key}
                        value={values.key}
                        placeholder="Key"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        className={touched.key && errors.key ? "has-error" : null}
                        id="key"
                      />
                    </Form.Group>

                    <Card centered fluid>
                      <UppyDashboard
                        height="300px"
                        width="890px"
                        uppy={uppy_Single}
                        theme="light"
                        showSelectedFiles
                        hideUploadButton
                        closeModalOnClickOutside
                        showProgressDetails
                      />
                    </Card>
                    <Button
                      type="submit"
                      floated="right"
                      disabled={uppy_Single.getFiles().length === 0}
                      color="green"
                    >
                      <Icon name="paper plane outline" /> Submit
                  </Button>
                    <Button
                      floated="right"
                      color="red"
                      onClick={() => {
                        setShowModel_single(false);
                        setFileModel({ name: "", date: "", key: "", file: null });
                        uppy_Single.cancelAll();
                      }}
                    >
                      <Icon name="close" />
                    Close
                  </Button>
                    <br />
                  </Form>
                </Modal.Description>
              </Modal.Content>
            </Modal>
          )}
      </Formik>
      {/* Batch Upload Uppy Dashboard */}
      <Modal
        title="Modal_Batch"
        open={showModel_batch}
        onOpen={() => setShowModel_batch(true)}
        onClose={() => {
          setShowModel_batch(false);
          setIsUploadingBatch(false);
          uppy_Batch.cancelAll();
          setIsUploadingBatch(false);
        }}
      >
        <Modal.Header>Upload your files</Modal.Header>
        <Modal.Content>
          <Modal.Description>
            <Form
              onSubmit={() => {
                submitHandlerForBatch();
              }}
            >
              <Card centered fluid>
                <UppyDashboard
                  height="300px"
                  width="890px"
                  uppy={uppy_Batch}
                  theme="light"
                  showSelectedFiles
                  hideUploadButton
                  showProgressDetails
                />
              </Card>
                <Button type="submit" floated="right" color="green" disabled={isUploadingBatch}>
                  { isUploadingBatch ? <Loader active size='mini' inline='centered'/> : <><Icon name="paper plane outline" /> Submit Batch</>} 
                </Button>
                <Button
                  floated="right"
                  color="red"
                  disabled={isUploadingBatch}
                  onClick={() => {setShowModel_batch(false);
                                  setIsUploadingBatch(false);}}
                >
                <Icon name="close" />
                Close
              </Button>
              <br />
            </Form>
          </Modal.Description>
        </Modal.Content>
      </Modal>
      <React.Fragment>
        <Grid>
          <Grid.Row columns={2}>
            <Grid.Column floated='left'><Header as='h1'>Dashboard</Header></Grid.Column>
            {isUserAdmin ? 
             <Grid.Column floated='right'>
             <Button
               color="black"
               floated='right'
               onClick={() => {
                 setShowModel_batch(true);
               }}
             >
               <Icon name="folder" />
               Batch Upload
             </Button>
             <Button
               color="linkedin"
               floated='right'
               onClick={() => {
                 setShowModel_single(true);
               }}
             >
               <Icon name="file pdf outline" />
               Single Upload
             </Button> 
            
           </Grid.Column>
            : ""}
           
          </Grid.Row>
          <Grid.Row columns={2}>
            <Grid.Column width={13}>
              <DataTable
                title={"Records"}
                columns={columns}
                data={searchableList}
                highlightOnHover
                pagination
              />
            </Grid.Column>
            <Grid.Column width={3} style={{ marginTop: '5%' }}>
              <Header as='h3'>Search By:</Header>
              <Header as='h5'>Date Range (From-To)</Header>
              <Form onSubmit={(e) => filterRecord(e)}>
                <DateInput
                  size="small"
                  name="date"
                  placeholder="YYYY-MM-DD"
                  value={filterDateRange.lower}
                  dateFormat={"YYYY-MM-DD"}
                  popupPosition="right center"
                  onChange={(e, data) =>
                    setFilterDateRange({ ...filterDateRange, lower: data.value })
                  }
                  closable
                />
                <br />
                <DateInput
                  size="small"
                  name="date"
                  placeholder="YYYY-MM-DD"
                  value={filterDateRange.upper}
                  dateFormat={"YYYY-MM-DD"}
                  popupPosition="left center"
                  onChange={(e, data) =>
                    setFilterDateRange({ ...filterDateRange, upper: data.value })
                  }
                  closable
                />

                <Header as='h5'>Filter</Header>
                <Input
                  placeholder="Search..."
                  value={filters.name}
                  onChange={(e, data) =>
                    setFilters({ ...filters, name: data.value })
                  }
                />
                <br />
                <br />
                <DateInput
                  size="small"
                  name="date"
                  placeholder="YYYY-MM-DD"
                  value={filters.date}
                  dateFormat={"YYYY-MM-DD"}
                  popupPosition="right center"
                  onChange={(e, data) =>
                    setFilters({ ...filters, date: data.value })
                  }
                  closable
                />
                <br />

                <Button
                  size="mini"
                  floated='right'
                  color="youtube"
                  type="reset"
                  onClick={() => {
                    setSearchableList(recordList);
                    setFilters({ name: "", date: "" });
                    setFilterDateRange({ lower: "", upper: "" });
                  }}
                >
                  <Icon name="trash" />Reset
                </Button>
                  <Button
                    size="mini"
                    color="linkedin"
                    floated='left'
                    type="submit"
                >
                  <Icon name="search" />Search
                </Button>
              </Form>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </React.Fragment>
    </Container>
  );
};
export default Dashboard;
