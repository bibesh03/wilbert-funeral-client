import React, { useState, useEffect } from 'react';
import { Formik } from "formik";
import Validation from '../../_helpers/validators/Users/UserLoginValidation.js';
import { login } from '../../_services/userService';
import { Button, Form, Message, Card, Divider } from 'semantic-ui-react'
import { Url } from 'uppy';

const Login = (props) => {
  const[user, setUser] = useState({email:"", password:""});
  const[loginError, setLoginError] = useState("");

  useEffect(() => {
  }, []);

  const AuthenticateUser = (userInfo, setSubmitting) => {
    login(userInfo).then(res => {
      setSubmitting(false);
      if (typeof res === "string") {
        setLoginError(res);
      } else {
        props.history.push("/");
      }
    }
    ).catch(err => {
      setLoginError("Something went wrong with the network! Please try again later" );
      setSubmitting(false);
    });
  }

  return (
    <div style={{ height: '100vh',
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center', 
                  background: 'url("https://images.pexels.com/photos/257360/pexels-photo-257360.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260")',
                  backgroundPosition: 'center',
                  backgroundSize: 'cover',
                  backgroundRepeat: 'none'
                }}
    > <Card style={{ minWidth: '500px' }} centered>
      <Card.Content>
        <Card.Header style={{textAlign: 'center', fontSize: '44px', fontFamily: 'auto' }}>Wilbert Funeral Homes</Card.Header>
        <Card.Meta style={{textAlign: 'center'}}>
          <span>Our Family serving your family for 170 years</span>
        </Card.Meta>
        <Divider/>
        <Card.Header>Login</Card.Header>
        <Card.Meta>Please enter your credentials</Card.Meta>
      </Card.Content>
      
      <Card.Content>
        {loginError === "" ? "" : <Message negative>
          <Message.Header>Login Failed</Message.Header>
          <p>{loginError}</p> 
        </Message>}
      <Formik initialValues={user}
          validationSchema={Validation}
          onSubmit={(values, { setSubmitting }) => {
            setUser(values);
            AuthenticateUser(values, setSubmitting);
          }}
        >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
        }) => (
          <div>
                <Form onSubmit={handleSubmit}>
                  <Form.Group widths='equal'>

                    <Form.Field>
                      <label>Email/Username</label>
                      <Form.Input 
                        placeholder="Enter username or email" 
                        id="email"
                        onChange={handleChange}
                        value={values.email}
                        onBlur={handleBlur}
                        style={{ minWidth: '416.11px'}}
                        icon='user'
                        iconPosition='left'
                        error={touched.email && errors.email}
                      />
                    </Form.Field>
                  </Form.Group>

                  <Form.Group widths='equal'>
                    <Form.Field>
                      <label>Password</label>
                      <Form.Input  
                        id='password'
                        placeholder='Enter password'
                        onChange={handleChange}
                        value={values.password}
                        type='password'
                        icon='key'
                        iconPosition='left'
                        onBlur={handleBlur}
                        error={touched.password && errors.password}
                        style={{ minWidth: '416.11px'}}
                      />
                    </Form.Field>
                  </Form.Group>
                  <br/>
                  <Button fluid type='submit' disabled={isSubmitting} positive>Log In</Button>
                </Form>
          </div>
          )}
      </Formik>
      </Card.Content>
    </Card>
    </div>
  );
}

export default Login;
