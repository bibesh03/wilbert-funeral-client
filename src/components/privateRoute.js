import React, { useState, useEffect } from 'react';
import { Route, Redirect } from 'react-router-dom';
import NavBar from './Navbar';
import { getCurrentUser, isUserLoggedIn } from '../_services/userService';

export const PrivateRoute = ({ component: Component, ...rest }) =>  {
    const[isAuth, setIsAuth] = useState(false);
    const[hitEndpoint, setHitEndpoint] = useState(false);

    return(
    <Route {...rest} render={props => {
        const currentUser = getCurrentUser();

        if (!currentUser || !currentUser.token) {
            // not logged in so redirect to login page with the return url
            return <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
        }

        isUserLoggedIn().then(res => {
            setIsAuth(res);
            setHitEndpoint(true);
        }).catch(() => {
            setHitEndpoint(true);
        });

        return isAuth ? (
            // authorised so return component
            <React.Fragment>
                <NavBar {...props}/>
                <Component {...props} />
            </React.Fragment>
        ) : <LoadingScreen isAuth={isAuth} hitEndpoint={hitEndpoint}/>
        
    }} />
)}

const LoadingScreen = (props) => {
    return props.hitEndpoint && !props.isAuth ? <Redirect to={{ pathname: '/login', state: { from: props.location } }} /> : (
        <div>
            <p>Loading...</p>
        </div>
    )
}