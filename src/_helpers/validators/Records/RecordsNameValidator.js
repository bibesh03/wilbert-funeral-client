import * as Yup from "yup";

export default Yup.object().shape({
    name: Yup.string()
    .required("Name is required"),
    key: Yup.string()
    .required('Key is required'),
});
  

