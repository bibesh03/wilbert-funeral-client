import 'react-app-polyfill/stable';
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Login from './components/Login/Login';
import Register from './components/Register/Register';
import Dashboard from './components/Dashboard';
import UserEdit from './components/User/UserEdit';
import RecordEdit from './components/Record/RecordEdit';
import RecordFile from './components/Record/RecordFile';
import * as serviceWorker from './serviceWorker';
import { Router, Route, Switch } from "react-router-dom";
import { createBrowserHistory } from "history";
import { PrivateRoute } from './components/PrivateRoute';
import UserListing from './components/User/UserListing';
import {ToastContainer} from 'react-toastify';

import 'react-toastify/dist/ReactToastify.css';

const hist = createBrowserHistory();

ReactDOM.render(
  <Router history={hist}>
      <Switch>
        <Route path="/Login" component={Login}/>
        <PrivateRoute exact path="/" component={Dashboard}/>
        <PrivateRoute path="/Register" component={Register}/>
        <PrivateRoute path="/Manage" component={UserListing}/>
        <PrivateRoute path="/User/:id" component={UserEdit}/>
        <PrivateRoute path="/Record/:id" component={RecordEdit}/>
        <PrivateRoute path="/File/:id" component={RecordFile}/>
      </Switch>
      <ToastContainer
        position="top-right"
        autoClose={3500}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        draggable
      />
  </Router>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
