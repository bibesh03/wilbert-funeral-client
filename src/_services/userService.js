import constants from '../_constants/constant'
import axios from "axios"
import authHeader from "../_services/auth-headers";

const login = (user) =>  axios
                        .post(constants.API_URL + "Users/authenticate", user)
                        .then(response => response.data)
                        .then(response => {
                          if (response.message) {
                            return response.message;
                          }
                          if (response.token) {
                            localStorage.setItem("user", JSON.stringify(response));
                          }

                          return response;
                        });
    
const register = (user) => axios({
                              'method': 'post',
                              'url': constants.API_URL + "Users/register",
                              'headers': authHeader(),
                              'data': user
                            })
                            .then(response => response.data)
                            .then(response => {
                              return response;
                            })
                            .catch(err => {
                              window.location = window.origin;
                              return false;
                            });

const getUserId = (userId) => axios({
                                'method':'get',
                                'url': constants.API_URL + 'Users/edit/' +  userId,
                                'headers': authHeader(),
                                })
                                .then(response => response.data)
                                .then(response => {
                                  return response;
                                })
                                .catch(err => {
                                  return false;
                                });

 const editUser = (user) => axios({
                                  'method': 'post',
                                  'url': constants.API_URL + 'Users/edit',
                                  'headers': authHeader(),
                                  'data': user
                                })
                                .then(response => response.data)
                                .then(response => {
                                  return response;
                                })
                                .catch(err => {
                                  window.location = window.origin;
                                  return false;
                                });
                         
const resetPassword = (userId) => axios({
                                  'method': 'post',
                                  'url': constants.API_URL + 'Users/changePassword/' + userId,
                                  'headers': authHeader(),
                                })
                                .then(response => response.data)
                                .then(response => {
                                  return response;
                                })
                                .catch(err => {
                                  window.location = window.origin;
                                  return false;
                                });

const getCurrentUser = () => JSON.parse(localStorage.getItem('user'));

const getAllUsers = () => axios({
                            'method': 'get',
                            'url': constants.API_URL + "Users/getAllUser",
                            'headers': authHeader()
                          })
                          .then(response => response.data)
                          .then(response => {
                            return response;
                          })
                          .catch(err => {
                            window.location = window.origin;
                            return false;
                          });

const isUserLoggedIn = () => axios({
                              'method': 'get',
                              'url': constants.API_URL + "Users/verify",
                              'headers': authHeader()
                            })
                            .then(response => response.data)
                            .then(response => {
                              return response;
                            })
                            .catch(err => {
                              return false;
                            });

const logOut = (id) => axios({
  'method': 'get',
  'url': constants.API_URL + "Users/logOut/" + id,
  'headers': authHeader()
})
.catch(err => {
  return false;
});


const uploadRecords = () => axios({
                            'method': 'post',
                            'url':constants.API_URL + 'Records/addRecords',
                            'headers':authHeader()
                            })
                            .then(response => response.data)
                            .then(response => {
                              return response;
                            })
                            .catch(err => {
                              return false;
                            });


export {
  login, 
  register, 
  getCurrentUser, 
  isUserLoggedIn,
  getAllUsers,
  getUserId,
  editUser,
  resetPassword,
  uploadRecords,
  logOut
};