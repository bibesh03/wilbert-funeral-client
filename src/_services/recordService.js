import constants from '../_constants/constant'
import axios from "axios"
import authHeader from "../_services/auth-headers";

const saveFileToServer = (record) => axios({
                                            'method': 'post',
                                            'url': constants.API_URL + "Records/AddRecords",
                                            'headers': {...authHeader(), 'Content-Type': 'multipart/form-data'},
                                            'data': record
                                          })
                                          .then(response => response.data)
                                          .then(response => {
                                            return response;
                                          })
                                          .catch(err => {
                                            return false;
                                          });

const getAllRecords = () => axios({
                                    'method': 'get',
                                    'url': constants.API_URL + "Records/getRecords",
                                    'headers': authHeader()
                                  })
                                  .then(response => response.data)
                                  .then(response => {
                                    return response;
                                  })
                                  .catch(err => {
                                    return false;
                                  });

const deleteRecord = (id) => axios({
                                    'method':'delete',
                                    'url': constants.API_URL + "Records/deleteRecord/" + id,
                                    'headers': authHeader()
                                  })
                                    .then(res => res.data)
                                    .then(res => {
                                      return res;
                                    })
                                    .catch(err=>{
                                      return false;
                                  });

const getRecord = (recordId) => axios({
                                      'method': 'get',
                                      'url': constants.API_URL + "Records/getRecordToEdit/" + recordId,
                                      'headers': authHeader()
                                    })
                                    .then(response => response.data)
                                    .then(response => {
                                      return response;
                                    })
                                    .catch(err => {
                                      return false;
                                    });

const editRecord = (record) => axios({
                                      'method': 'post',
                                      'url': constants.API_URL + 'Records/editRecord',
                                      'headers': authHeader(),
                                      'data': record
                                    })
                                    .then(response => response.data)
                                    .then(response => {
                                      return response;
                                    })
                                    .catch(err => {
                                      return false;
                                    });

const viewFile = (id) => axios({
                                'method': 'get',
                                'url':constants.API_URL + 'Records/file/'+id,
                                'headers':authHeader()

                                })
                                .then(response => response.data)
                                .then(response => {
                                  return response;
                                })
                                .catch(err => {
                                  return false;
                                });

export {
  saveFileToServer,
  getAllRecords,
  deleteRecord,
  getRecord,
  editRecord,
  viewFile
}